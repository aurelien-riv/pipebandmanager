import React from "react";
import {Box, Card, CardContent, CardMedia, Skeleton, Typography} from "@mui/material";
import {StaffApi} from "../service/staffapi";

type UserType = {
    avatar: string,
    firstname: string,
    lastname: string
}
type UserCardProps = {
    userId: number
}
type UserCardState = {
    user?: UserType
};

export class UserCard extends React.Component<UserCardProps, UserCardState> {
    static cache: Record<number, UserType> = {};

    constructor(props: UserCardProps) {
        super(props);

        if (UserCard.cache[this.props.userId] !== undefined) {
            this.state = {user: UserCard.cache[this.props.userId]};
        } else {
            this.state = {};
        }
    }

    componentDidMount() {
        if (UserCard.cache[this.props.userId] === undefined) {
            StaffApi.getUser(this.props.userId)
                .then(user => this.setState({user: user }))
                .catch(() => {});
        }
    }

    render() {
        if (this.state.user) {
            return this.renderInitialized();
        }
        return <Skeleton variant="rectangular" height={75} sx={{ mb: 3 }} animation="wave" />;
    }

    renderInitialized() {
        return <Card sx={{ display: 'flex', mb: 3 }}>
            <CardMedia
                component="img"
                sx={{ width: 75 }}
                image={`data:image/png;base64, ${this.state.user!.avatar}`}
            />
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="div" variant="h5">
                        {this.state.user!.firstname} {this.state.user!.lastname.toUpperCase()}
                    </Typography>
                </CardContent>
            </Box>
        </Card>;
    }
}