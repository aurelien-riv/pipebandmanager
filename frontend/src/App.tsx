import Dashboard from './dashboard/Dashboard';
import * as React from 'react';
import { Route, Switch } from "react-router-dom";
import {PbmPageSkillsContent, PbmPageSkillsFooter} from "./page/beginner/Skills";
import {PbmPageNotesContent, PbmPageNotesFooter} from "./page/beginner/Notes";
import {PbmPageStudents} from "./page/teacher/Students";
import {PbmPageAdminUsers} from "./page/admin/Users";
import {PbmPageProfile} from "./page/profile/Profile";

export default class extends React.Component {
    render() {
      return <Dashboard
          content={
            <Switch>
              <Route path="/" exact component={() => null} />
              <Route path="/profile/:userId" exact component={PbmPageProfile} />
              <Route path="/admin/users" exact component={() => <PbmPageAdminUsers/>} />
              <Route path="/teacher/students" exact component={() => <PbmPageStudents/>} />
              <Route path="/musicien/:musicienId/skills" exact component={() => <PbmPageSkillsContent/>} />
              <Route path="/musicien/:musicienId/notes" exact component={() => <PbmPageNotesContent/>} />
            </Switch>
          }
          footer={
            <Switch>
              <Route path="/musicien/:musicienId/skills" exact component={() => <PbmPageSkillsFooter/>} />
              <Route path="/musicien/:musicienId/notes" exact component={() => <PbmPageNotesFooter/>} />
            </Switch>
          }
        />
    }
}

