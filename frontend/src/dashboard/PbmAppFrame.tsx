import * as React from "react";
import {styled} from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Drawer from "@mui/material/Drawer";
import {PbmAppToolbar, PbmDrawerToolbar} from "./PbmToolbar";
import Divider from "@mui/material/Divider";
import {PbmMenuEntries} from "./PbmMenuEntries";

const drawerWidth = 260;

const PbmAppBar = styled(AppBar)(({theme}) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    '&.open': {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
}));

const PbmDrawer = styled(Drawer)(({theme}) => ({
    '& .MuiDrawer-paper': {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        boxSizing: 'border-box'
    },
    '&:not(.open) .MuiDrawer-paper': {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: 0,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(7),
        }
    },
}));

type PbmAppFrameState = {
    open: boolean
}
export class PbmAppFrame extends React.Component<Readonly<{}>, PbmAppFrameState> {
    constructor(props: Readonly<{}>) {
        super(props);
        this.state = {
            open: false
        }
    }

    render() {
        return <React.Fragment>
            <PbmAppBar position="absolute" className={this.state.open ? 'open' : ''}>
                <PbmAppToolbar toggleDrawer={this.toggleDrawer.bind(this)} open={this.state.open}/>
            </PbmAppBar>
            <PbmDrawer className={this.state.open ? 'open' : ''} variant="permanent">
                <PbmDrawerToolbar toggleDrawer={this.toggleDrawer.bind(this)}/>
                <Divider/>
                <PbmMenuEntries/>
            </PbmDrawer>
        </React.Fragment>;
    }

    toggleDrawer() {
        this.setState({
            open: ! this.state.open
        })
    }
}