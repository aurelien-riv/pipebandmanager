import * as React from "react";
import auth from "../service/auth";
import {Divider, List, ListItemIcon, ListItemText, ListItem} from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import BedroomBabyIcon from '@mui/icons-material/BedroomBaby';
import SchoolIcon from "@mui/icons-material/School";
import MusicNoteIcon from "@mui/icons-material/MusicNote";
import SettingsIcon from "@mui/icons-material/Settings";
import {Link} from "react-router-dom";

const mainMenuItems = (
    <React.Fragment>
        {auth.roles.includes('beginner') &&
            <ListItem button component={Link} to={`/musicien/${auth.user_id}/skills`}>
                <ListItemIcon>
                    <BedroomBabyIcon/>
                </ListItemIcon>
                <ListItemText primary="Espace débutant"/>
            </ListItem>
        }
        {auth.roles.includes('musician') &&
            <ListItem button>
                <ListItemIcon>
                    <MusicNoteIcon/>
                </ListItemIcon>
                <ListItemText primary="Espace musicien"/>
            </ListItem>
        }
        {auth.roles.includes('teacher') &&
            <ListItem button component={Link} to="/teacher/students">
                <ListItemIcon>
                    <SchoolIcon/>
                </ListItemIcon>
                <ListItemText primary="Espace formateur"/>
            </ListItem>
        }
        {auth.roles.includes('admin') &&
            <ListItem button component={Link} to="/admin/users">
                <ListItemIcon>
                    <SettingsIcon/>
                </ListItemIcon>
                <ListItemText primary="Espace administrateur"/>
            </ListItem>
        }
    </React.Fragment>
);

export class PbmMenuEntries extends React.Component {
    render() {
        return <React.Fragment>
            <List>{mainMenuItems}</List>
            <Divider/>
            <ListItem button onClick={this.onClickLogout.bind(this)}>
                <ListItemIcon>
                    <LogoutIcon/>
                </ListItemIcon>
                <ListItemText primary="Déconnexion"/>
            </ListItem>
        </React.Fragment>;
    }

    onClickLogout() {
        fetch(`/api/auth/logout`).then(() => {
            auth.forgetToken();
            window.location.replace('/login');
        })
    };
}