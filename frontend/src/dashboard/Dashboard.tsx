import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import {Container, Paper} from "@mui/material";
import {PbmAppFrame} from "./PbmAppFrame";

type DashboardContentProps = {
    content: React.ReactNode,
    footer?: React.ReactNode
}
function DashboardContent(props: DashboardContentProps) {
    return  <Box sx={{
            display: 'flex',
            mb: props.footer ? "1vh" : 0
        }}>
            <CssBaseline/>
            <PbmAppFrame/>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) => theme.palette.grey[100],
                    flexGrow: 1,
                    height: props.footer ? '99vh' : '100vh',
                    overflow: 'auto',
                }}
            >
                <Container maxWidth="xl" sx={{mt: 10, mb: 4}}>
                    {props.content}
                </Container>
            </Box>
            {props.footer &&
                <Paper sx={{position: 'fixed', bottom: 0, left: 0, right: 0}} elevation={3}>
                    {props.footer}
                </Paper>
            }
        </Box>;
}

export default function Dashboard(props: DashboardContentProps) {
    return <DashboardContent content={props.content} footer={props.footer}/>;
}
