import * as React from "react";
import {Avatar, Chip, Toolbar} from "@mui/material";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import auth from "../service/auth";
import {PbmTitle} from "./PbmTitle";
import {MouseEventHandler} from "react";
import {RouteComponentProps, withRouter} from "react-router-dom";

type PbmLoggedUserChipState = {
    id: number,
    name: string,
    avatar: string
}
class _PbmLoggedUserChip extends React.Component<RouteComponentProps, PbmLoggedUserChipState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            id: auth.user!.id,
            name: auth.user!.firstname,
            avatar: auth.user!.avatar
        };
    }

    render() {
        return <Chip
            avatar={<Avatar alt={this.state.name} src={`data:image/png;base64, ${this.state.avatar}`} />}
            label={this.state.name}
            onClick={() => this.props.history.push(`/profile/${this.state.id}`)}
            onDelete={this.onClickLogout.bind(this)}
            deleteIcon={<PowerSettingsNewIcon/>}
        />;
    }

    onClickLogout() {
        fetch(`/api/auth/logout`).then(() => {
            auth.forgetToken();
            window.location.replace('/login');
        })
    }
}
const PbmLoggedUserChip = withRouter(_PbmLoggedUserChip);

type PbmAppToolbarProps = {
    toggleDrawer: MouseEventHandler,
    open: boolean
};
export class PbmAppToolbar extends React.Component<PbmAppToolbarProps> {
    render() {
        return <Toolbar sx={{pr: '24px'}}>
            <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={this.props.toggleDrawer}
                sx={{
                    marginRight: '36px',
                    ...(this.props.open && {display: 'none'}),
                }}
            >
                <MenuIcon/>
            </IconButton>
            <PbmTitle/>
            <PbmLoggedUserChip/>
        </Toolbar>;
    }
}

type PbmDrawerToolbarProps = {
    toggleDrawer: MouseEventHandler
};
export class PbmDrawerToolbar extends React.Component<PbmDrawerToolbarProps> {
    render() {
        return <Toolbar
            sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                px: [1],
            }}
        >
            <IconButton onClick={this.props.toggleDrawer}>
                <ChevronLeftIcon/>
            </IconButton>
        </Toolbar>;
    }
}