import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

export class NewUserDialog extends React.Component {
    render() {
        return <Dialog open={this.props.open} onClose={this.props.onClose}>
            <DialogTitle>New user</DialogTitle>
            <DialogContent>
                <form id="admin.newuserform" onSubmit={this.onSubmit.bind(this)}>
                    <TextField name="firstname" label="Firstname" variant="standard" required sx={{mx:1}} />
                    <TextField name="lastname"  label="Lastname"  variant="standard" required sx={{mx:1}}  />
                    <TextField name="email"     label="Email"     variant="standard" required sx={{mx:1}} type="email" />
                    <TextField name="phone"     label="Phone"     variant="standard" sx={{mx:1}} type="number" />
                </form>
            </DialogContent>
            <DialogActions>
                <Button onClick={this.props.onClose}>Cancel</Button>
                <Button type="submit" form="admin.newuserform">Create</Button>
            </DialogActions>
        </Dialog>;
    }

    onSubmit(evt) {
        evt.preventDefault();
        const data = new FormData(evt.target);
        this.props.onSubmit(data);
        evt.target.reset();
    }
}