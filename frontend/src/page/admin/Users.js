import * as React from 'react';
import {Chip, Fab, FormControl, MenuItem, OutlinedInput, Select, Box} from "@mui/material";
import {DataGrid, GridActionsCellItem} from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import BlockIcon from '@mui/icons-material/Block';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import AddIcon from '@mui/icons-material/Add';
import {NewUserDialog} from "./NewUserDialog";
import {PbmTitle} from "../../dashboard/PbmTitle";
import {StaffApi} from "../../service/staffapi";

class PbmRolesSelector extends React.Component {
    render() {
        return <FormControl sx={{ m: 1, width: 300 }}>
            <Select
                labelId="demo-multiple-chip-label"
                id="demo-multiple-chip"
                multiple
                value={this.props.value || []}
                onChange={this.onRolesChange.bind(this)}
                input={<OutlinedInput id="select-multiple-chip" />}
                renderValue={(selected) => (
                    <Box sx={{ display: 'flex', gap: 0.5, overflow: 'hidden' }}>
                        {selected.map((value) => (
                            <Chip key={value} label={this.props.roles.find(r => r.code === value).name} />
                        ))}
                    </Box>
                )}
            >
                {this.props.roles.map((role) => (
                    <MenuItem key={role.id} value={role.code}>
                        {role.name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>;
    }

    onRolesChange(evt) {
        StaffApi.setUserRoles(this.props.userId, evt.target.value, this.props.roles);
        this.props.onRolesChange(evt.target.value);
    }
}

export class PbmPageAdminUsers extends React.Component {
    columns = [
        { field: 'firstname', headerName: 'Firstname', flex: 1 },
        { field: 'lastname', headerName: 'Lastname', flex: 1 },
        { field: 'email', headerName: 'Adresse e-mail', flex: 1 },
        { field: 'phone', headerName: 'Téléphone', flex: 1 },
        {
            field: 'roles',
            headerName: 'Roles',
            flex: 1,
            renderCell: (params) => <PbmRolesSelector
                userId={params.row.id}
                onRolesChange={this.onRolesChange.bind(this, params.row.id)}
                roles={this.state.roles || []}
                value={params.value}
            />,
        },
        {
            field: 'actions',
            type: 'actions',
            getActions: (params) => [
                <GridActionsCellItem icon={<OpenInNewIcon/>} label="Open" onClick={() => window.location.replace(`/profile/${params.id}`)}/>,
                <GridActionsCellItem icon={<BlockIcon/>} label="Disable" onClick={this.onDisableUser.bind(this, params.id)} showInMenu/>,
                <GridActionsCellItem icon={<DeleteIcon/>} label="Delete" onClick={this.onDeleteUser.bind(this, params.id)} showInMenu/>,
            ]
        }
    ];

    constructor(props) {
        super(props);
        this.state = {
            showNewUserDialog: false,
            users: [],
            roles: []
        }
    }

    componentDidMount() {
        PbmTitle.updateMainTitle("Administration > Membres");
        StaffApi.getUsers()
            .then(users => this.setState({ users: users }))
            .catch(() => {});
        StaffApi.getRoles()
            .then(roles => this.setState({ roles: roles }))
            .catch(() => {});
    }

    render() {
        return <div style={{ height: 400, width: '100%' }}>
            <DataGrid rows={this.state.users} columns={this.columns} hideFooterSelectedRowCount />
            <Fab color="primary" aria-label="add" sx={{position: 'absolute', bottom: 32, right: 32}} onClick={() => this.setState({showNewUserDialog: true})}>
                <AddIcon />
            </Fab>
            <NewUserDialog
                open={this.state.showNewUserDialog}
                onClose={() => this.setState({showNewUserDialog: false})}
                onSubmit={this.onCreateUser.bind(this)}
            />
        </div>;
    }

    onCreateUser(formdata) {
        StaffApi.createUser(formdata)
            .then(newUser => {
                const users = [...this.state.users];
                users.push(newUser);
                this.setState({
                    users: users,
                    showNewUserDialog: false
                })
            })
            .catch(() => {});
    }

    onDeleteUser(userId) {
        StaffApi.deleteUser(userId)
            .then(() => {
                const users = [...this.state.users];
                const idxToRemove = users.findIndex(e => e.id === userId);
                users.splice(idxToRemove, 1);
                this.setState({
                    users: users
                })
            })
            .catch(() => {});
    }

    onDisableUser(userId) {
        StaffApi.disableUser(userId);
    }

    onRolesChange(userId, newValue) {
        const users = [...this.state.users];
        const idx = users.findIndex(e => e.id === userId);
        users[idx].roles = newValue;
        this.setState({
            users: users
        })
    }
}