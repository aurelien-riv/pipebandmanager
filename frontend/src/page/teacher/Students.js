import React from "react";
import {PbmTitle} from "../../dashboard/PbmTitle";
import {Avatar, List, ListItemAvatar, ListItemButton, ListItemText} from "@mui/material";
import {Link} from "react-router-dom";

export class PbmPageStudents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            students: []
        }
    }

    componentDidMount() {
        PbmTitle.updateMainTitle("Formateur > Élèves");
        fetch('/api/beginners/students', {
            credentials: "include"
        })
            .then(payload => payload.json())
            .then(payload => this.setState({
                students: payload
            }))
    }

    render() {
        return <List sx={{ width: '100%' }}>
            {this.state.students.map(s =>
                <ListItemButton key={s.id} component={Link} to={`/musicien/${s.id}/skills`}>
                    <ListItemAvatar>
                        <Avatar src={`data:image/png;base64, ${s.avatar}`} />
                    </ListItemAvatar>
                    <ListItemText primary={`${s.firstname} ${s.lastname.toUpperCase()}`}/>
                </ListItemButton>
            )}
        </List>;
    }
}