import React, {ChangeEvent} from "react";
import {StaffApi} from "../../service/staffapi";
import {Avatar, Box, Button, Paper, Skeleton} from "@mui/material";

export class PbmAvatarSelector extends React.Component<Readonly<{userId: string}>, {avatar: string}> {
    componentDidMount() {
        StaffApi.getUser(this.props.userId)
            .then(user => this.setState({avatar: `data:image/png;base64, ${user.avatar}` }))
            .catch(() => {});
    }

    render() {
        if (! this.state) {
            return <Box><Skeleton variant="rectangular" width={100} height={110} sx={{ m:"auto"}} /></Box>;
        }
        return <Box textAlign="center">
            <input accept="image/*" id="upload-avatar" type='file' hidden onChange={this.onAvatarChange.bind(this)} />
            <label htmlFor="upload-avatar">
                <Button component="span">
                    <Paper elevation={5}>
                        <Avatar src={this.state.avatar} sx={{ width: 100, height: 100 }} variant='rounded' />
                    </Paper>
                </Button>
            </label>
        </Box>
    }

    onAvatarChange(evt: ChangeEvent<HTMLInputElement>) {
        if (evt.target.files && evt.target.files[0]) {
            const file = evt.target.files[0];

            fetch(`/api/staff/${this.props.userId}/profile/avatar`, {
                method: 'POST',
                body: file
            }).then(() => {
                let reader = new FileReader();
                reader.onloadend = () => this.setState({avatar: reader.result as string});
                reader.readAsDataURL(file);
            });
        }
    }
}
