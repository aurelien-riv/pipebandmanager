import React from "react";
import {Box, Container, Grid, Tab} from "@mui/material";
import {TabContext, TabList, TabPanel} from "@mui/lab";
import {RouteComponentProps} from "react-router-dom";
import {PbmProfileBasicInformation} from "./BasicInfoForm";
import {PbmAvatarSelector} from "./AvatarForm";
import {PbmProfilePassword} from "./Password";

export class PbmPageProfile extends React.Component<RouteComponentProps<{userId: string}>, {currentTab: string}> {
    constructor(props: RouteComponentProps<{userId: string}>) {
        super(props);
        this.state = {
            currentTab: "0"
        }
    }

    onTabChange(evt: any, newValue: string) {
        this.setState({
            currentTab: newValue
        });
    }

    render() {
        return <Container component="main" maxWidth="md">
            <Grid item xs={12}>
                <PbmAvatarSelector userId={this.props.match.params.userId}/>
            </Grid>
            <Box sx={{ width: '100%' }}>
                <TabContext value={this.state.currentTab}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList
                            variant="scrollable"
                            scrollButtons="auto"
                            onChange={this.onTabChange.bind(this)}
                        >
                            <Tab label="Mon compte" value="0" />
                            <Tab label="Mot de passe" value="1" />
                        </TabList>
                    </Box>

                    <TabPanel value="0">
                        <PbmProfileBasicInformation userId={this.props.match.params.userId}/>
                    </TabPanel>
                    <TabPanel value="1">
                        <PbmProfilePassword userId={this.props.match.params.userId}/>
                    </TabPanel>
                </TabContext>
            </Box>
        </Container>;
    }
}