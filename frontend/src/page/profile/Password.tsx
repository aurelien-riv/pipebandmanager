import React from "react";
import {Box, Button, Grid, LinearProgress, TextField} from "@mui/material";

type PbmProfilePasswordState = {
    strength: number,
    password: string,
    confirm: string
}
export class PbmProfilePassword extends React.Component<{ userId: string }, PbmProfilePasswordState> {
    static INITIAL_STATE = {
        strength: 0,
        password: '',
        confirm: ''
    };
    constructor(props: { userId: string }) {
        super(props);
        this.state = PbmProfilePassword.INITIAL_STATE;
    }

    handleSubmit(evt: React.FormEvent<HTMLFormElement>) {
        evt.preventDefault();
        fetch(`/api/staff/${this.props.userId}/profile/password`, {
            method: 'POST',
            body: this.state.password
        }).then(() => this.setState(PbmProfilePassword.INITIAL_STATE));
    }

    onPasswordChange(evt: React.ChangeEvent<HTMLInputElement>) {
        this.setState({password: evt.target.value});
        import('zxcvbn').then(zxcvbn => this.setState({strength: zxcvbn.default(evt.target.value).score}));
    }

    render() {
        return <Box component="form" noValidate onSubmit={this.handleSubmit.bind(this)} sx={{mt: 3}}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        required
                        fullWidth
                        value={this.state.password}
                        onChange={this.onPasswordChange.bind(this)}
                        label="Password"
                        name="password"
                        type="password"
                        error={this.state.password.length > 0 && this.state.strength < 3}
                        helperText={this.state.password.length > 0 && this.state.strength < 3 && "Password too weak"}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        fullWidth
                        value={this.state.confirm}
                        onChange={e => this.setState({confirm: e.target.value})}
                        label="Confirm"
                        name="confirm"
                        type="password"
                        error={this.state.confirm.length > 0 && this.state.password !== this.state.confirm}
                        helperText={this.state.confirm.length > 0 && this.state.password !== this.state.confirm && "Confirmation and password don't match"}
                    />
                </Grid>
                <Grid item xs={12}>
                    <LinearProgress
                        variant="determinate"
                        value={this.state.strength * 100 / 4}
                        color={this.state.strength >= 3 ? 'success' : 'error'}
                    />
                </Grid>
            </Grid>
            <Button type="submit"
                    disabled={this.state.strength < 3 || this.state.password !== this.state.confirm}
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
            >Save</Button>
        </Box>;
    }
}
