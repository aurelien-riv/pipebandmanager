import React from "react";
import {StaffApi} from "../../service/staffapi";
import {Box, Button, Grid, Skeleton, TextField} from "@mui/material";

type PbmProfileBasicInformationState = {
    user?: {
        avatar: string,
        firstname: string,
        lastname: string,
        email: string
    }
}

export class PbmProfileBasicInformation extends React.Component<Readonly<{ userId: string }>, PbmProfileBasicInformationState> {
    componentDidMount() {
        StaffApi.getUser(this.props.userId)
            .then(user => this.setState({user: user}))
            .catch(() => {});
    }

    handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        fetch(`/api/staff/${this.props.userId}/profile/basic`, {
            method: 'POST',
            body: JSON.stringify({
                firstname: data.get('firstname'),
                lastname: data.get('lastname'),
                email: data.get('email')
            })
        });
    }

    render() {
        if (!this.state) {
            return <Skeleton variant="rectangular" width={800} height={200} sx={{mb: 3}} animation="wave"/>;
        }

        return <Box component="form" noValidate onSubmit={this.handleSubmit.bind(this)} sx={{mt: 3}}>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        autoComplete="fname"
                        name="firstname"
                        required
                        fullWidth
                        label="First Name"
                        autoFocus
                        defaultValue={this.state.user!.firstname}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        fullWidth
                        label="Last Name"
                        name="lastname"
                        autoComplete="lname"
                        defaultValue={this.state.user!.lastname}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        fullWidth
                        label="Email Address"
                        type="email"
                        name="email"
                        autoComplete="email"
                        defaultValue={this.state.user!.email}
                    />
                </Grid>
            </Grid>
            <Button type="submit" fullWidth variant="contained" sx={{mt: 3, mb: 2}}>Save</Button>
        </Box>;
    }
}
