import React from "react";
import {BottomNavigation, BottomNavigationAction} from "@mui/material";
import {Link, withRouter} from "react-router-dom";
import PlaylistAddCheckIcon from "@mui/icons-material/PlaylistAddCheck";
import CommentIcon from "@mui/icons-material/Comment";

export class _PbmPageFooter extends React.Component {
    render() {
        return (
            <BottomNavigation
                value={this.props.activeTab}
                showLabels
            >
                <BottomNavigationAction
                    component={Link}
                    to={`/musicien/${this.props.match.params.musicienId}/skills`}
                    label="Compétences"
                    icon={<PlaylistAddCheckIcon />}
                />
                <BottomNavigationAction
                    component={Link}
                    to={`/musicien/${this.props.match.params.musicienId}/notes`}
                    label="Suivi"
                    icon={<CommentIcon />}
                />
            </BottomNavigation>
        );
    }
}

export const PbmPageFooter = withRouter(_PbmPageFooter);