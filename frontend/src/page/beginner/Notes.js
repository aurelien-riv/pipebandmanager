import React from "react";
import {withRouter} from "react-router-dom";
import {PbmPageFooter} from "./Footer";
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import TimelineDot from '@mui/lab/TimelineDot';
import Typography from '@mui/material/Typography';
import {Paper} from "@mui/material";
import {PbmTitle} from "../../dashboard/PbmTitle";
import {UserCard} from "../../common/UserCard";
import MUIRichTextEditor from "mui-rte";
import auth from "../../service/auth";

class PbmNoteReader extends React.Component {
    render() {
        return <MUIRichTextEditor
            defaultValue={this.props.value}
            readOnly={true}
            controls={[]}
        />;
    }
}

class PbmNoteWriter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nId: props.nId,
            value: props.value
        }
    }
    render() {
        return <MUIRichTextEditor
            label="Ce qui a été vu, ce qu'il faut travailler pour le prochain cours, remarques..."
            defaultValue={this.state.value}
            controls={["title", "bold", "italic", "underline", "strikethrough", "bulletList", "undo", "redo", "save"]}
            onSave={this.onSave.bind(this)}
        />;
    }

    async onSave(e) {
        if (this.state.nId === undefined) {
            fetch(`/api/beginners/${this.props.musicienId}/notes`, {
                method: 'PUT',
                body: e
            })
                .then(payload => payload.json())
                .then(payload => {
                    if (typeof this.props.onNoteAdded !== "undefined") {
                        this.props.onNoteAdded(payload);
                        this.setState({
                            nId: payload.id,
                            value: e
                        });
                    }
                });
        } else {
            fetch(`/api/beginners/${this.props.musicienId}/note/${this.state.nId}`, {
                method: 'PATCH',
                body: e
            });
        }
    }
}

class PbmNoteTimelineItem extends React.Component {
    render() {
        return <TimelineItem>
            <TimelineOppositeContent
                sx={{ m: 'auto 0' }}
                style={{ flex: 0.06 }}
                align="right"
                variant="body2"
                color="text.secondary"
            >
                {this.props.opposite}
            </TimelineOppositeContent>
            <TimelineSeparator>
                <TimelineConnector />
                <TimelineDot/>
                <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent sx={{ py: '12px', px: 2 }}>
                {this.props.children}
            </TimelineContent>
        </TimelineItem>;
    }
}

class PbmNote extends React.Component {
    render() {
        return <PbmNoteTimelineItem opposite={this.props.date}>
            <Paper elevation={3} sx={{p: 5}}>
                <Typography variant="h6" component="span">
                    {this.props.teacher}
                </Typography>
                {this.props.teacher.id === auth.user_id
                    ? <PbmNoteReader musicienId={this.props.musicienId} value={this.props.message}/>
                    : <PbmNoteWriter musicienId={this.props.musicienId} value={this.props.message} nId={this.props.nId}/>
                }
            </Paper>
        </PbmNoteTimelineItem>;
    }
}
class PbmNotesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: []
        }
    }

    componentDidMount() {
        fetch(`/api/beginners/${this.props.musicienId}/notes`)
            .then(payload => payload.json())
            .then(payload => this.setState({
                notes: payload
            }));
    }

    onNoteAdded(n) {
        const notes = this.state.notes;
        notes.unshift(n);
        this.setState({
            notes: notes
        });
    }

    render() {
        return (
            <Timeline>
                <PbmNoteTimelineItem opposite="">
                    <Paper elevation={3} sx={{p: 3, minHeight: 150 }}>
                        <Typography variant="h6" component="span">
                            {this.props.teacher}
                        </Typography>
                        <PbmNoteWriter musicienId={this.props.musicienId} onNoteAdded={this.onNoteAdded.bind(this)}/>
                    </Paper>
                </PbmNoteTimelineItem>
                {this.state.notes.map(n => <PbmNote
                    key={n.id}
                    nId={n.id}
                    date={n.date}
                    message={n.message}
                    musicienId={this.props.musicienId}
                    teacher={n.teacher.firstname}
                />)}
            </Timeline>
        );
    }
}

export class _PbmPageNotesContent extends React.Component {
    componentDidMount() {
        PbmTitle.updateMainTitle("Débutants > Suivi");
    }

    render() {
        return <React.Fragment>
            <UserCard userId={this.props.match.params.musicienId}/>

            <PbmNotesList musicienId={this.props.match.params.musicienId}/>
        </React.Fragment>;
    }
}
export const PbmPageNotesContent = withRouter(_PbmPageNotesContent);

export class PbmPageNotesFooter extends React.Component {
    render() {
        return <PbmPageFooter activeTab={1}/>;
    }
}