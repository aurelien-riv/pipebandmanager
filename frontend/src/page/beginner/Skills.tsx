import React from "react";
import {Card, CardContent, Box, Grid, Slider, Tab, Typography} from "@mui/material";
import './Skills.css';
import {RouteComponentProps, withRouter} from "react-router-dom";
import {PbmPageFooter} from "./Footer";
import {PbmTitle} from "../../dashboard/PbmTitle";
import {UserCard} from "../../common/UserCard";
import auth from "../../service/auth";
import {TabContext, TabList, TabPanel} from "@mui/lab";

class PbmSkillSlider extends React.Component<Readonly<{
    value: number,
    onSkillChange: (event: React.SyntheticEvent | Event, value: number | number[]) => void
}>> {
    render() {
        return (
            <Slider
                defaultValue={this.props.value}
                valueLabelDisplay="auto"
                marks
                min={0}
                max={4}
                disabled={!auth.roles.includes('teacher')}
                onChangeCommitted={this.props.onSkillChange}
            />
        );
    }
}

type PbmSkillProps = Readonly<{
    musicienId: number,
    skillId: number,
    name: string,
    level: number
}>;
type PbmSkillState = {
    level: number
}
class PbmSkill extends React.Component<PbmSkillProps, PbmSkillState> {
    constructor(props: PbmSkillProps) {
        super(props);
        this.state = {
            level: props.level
        };
    }
    render() {
        return (
            <Grid item xs={12} md={6} lg={3}>
                <Card sx={{ minWidth: 275 }}>
                    <CardContent className={`skill-card skill-card-${this.state.level}`}>
                        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            {this.props.name}
                        </Typography>
                        <PbmSkillSlider value={this.props.level} onSkillChange={this.onSkillChange.bind(this)}/>
                    </CardContent>
                </Card>
            </Grid>
        );
    }

    onSkillChange(evt: React.SyntheticEvent | Event, value: number | number[]) {
        this.setState({
            level: value as number
        });

        fetch(`/api/beginners/${this.props.musicienId}/skill/${this.props.skillId}/level/${value as number}`, {
            credentials: "include",
            method: 'POST'
        });
    }
}

type PbmSkillListProps = Readonly<{
    musicienId: number
}>
type PbmSkillListState = {
    currentTab: string,
    skillGroups: Array<{
        group_id: number,
        group_name: string,
        skills: Array<{
            skill_id: number,
            skill_name: string,
            level: number
        }>
    }>
}
export class PbmSkillList extends React.Component<PbmSkillListProps, PbmSkillListState> {
    constructor(props: PbmSkillListProps) {
        super(props);
        this.state = {
            skillGroups: [],
            currentTab: "0"
        }
    }

    componentDidMount() {
        fetch(`/api/beginners/${this.props.musicienId}/skills`, {
            credentials: "include"
        })
            .then(payload => payload.json())
            .then(payload => this.setState({
                currentTab: payload[0].group_id.toString(),
                skillGroups: payload
            }));
    }

    render() {
        return (
            <Box sx={{ width: '100%' }}>
                <TabContext value={this.state.currentTab}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList
                            variant="scrollable"
                            scrollButtons="auto"
                            onChange={this.onTabChange.bind(this)}
                        >
                            {this.state.skillGroups.map(sg => <Tab key={sg.group_id} label={sg.group_name} value={sg.group_id.toString()} />)}
                        </TabList>
                    </Box>

                    {this.state.skillGroups.map(sg =>
                        <TabPanel key={sg.group_id} value={sg.group_id.toString()}>
                            <Grid container spacing={3}>
                                {sg.skills.map(s =>
                                    <PbmSkill key={s.skill_id} skillId={s.skill_id} name={s.skill_name} musicienId={this.props.musicienId} level={s.level}/>
                                )}
                            </Grid>
                        </TabPanel>
                    )}
                </TabContext>
            </Box>
        );
    }

    onTabChange(evt: any, newValue: string) {
        this.setState({
            currentTab: newValue
        });
    }
}

class _PbmPageSkillsContent extends React.Component<RouteComponentProps<{musicienId: string}>> {
    componentDidMount() {
        PbmTitle.updateMainTitle("Débutants > Compétences");
    }

    render() {
        return <React.Fragment>
            <UserCard userId={parseInt(this.props.match.params.musicienId)}/>
            <PbmSkillList musicienId={parseInt(this.props.match.params.musicienId)}/>
        </React.Fragment>;
    }
}
export const PbmPageSkillsContent = withRouter(_PbmPageSkillsContent);

export class PbmPageSkillsFooter extends React.Component {
    render() {
        return <PbmPageFooter activeTab={0}/>;
    }
}