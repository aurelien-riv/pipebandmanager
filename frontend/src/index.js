import React from 'react';
import ReactDOM from 'react-dom';
import { SnackbarProvider } from 'notistack';
import {BrowserRouter as Router} from "react-router-dom";
import {createTheme, ThemeProvider} from '@mui/material/styles';
import auth from './service/auth';
import {SnackbarManager} from "./common/SnackBarManager";
import {Login} from "./page/Login";
const App = React.lazy(() => import('./App'));

const theme = createTheme();

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <Router>
                <React.Suspense fallback={null}>
                    {auth.isLoggedIn ? <App/> : <Login/>}
                </React.Suspense>
            </Router>
            <SnackbarProvider maxSnack={3}>
                <SnackbarManager />
            </SnackbarProvider>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

