import {BackendApi} from "./BackendApi";

type ApiRole = {
    id: number,
    code: string,
    name: string
}
type ApiUser = {
    id: number,
    avatar: string,
    firstname: string,
    lastname: string,
    email: string,
    phone: string|null
};

type ApiUserWithRoles = {
    user: ApiUser,
    roles: ApiRole[]
}

export class StaffApi {
    static async getUser(userId: string|number): Promise<ApiUser> {
        const user = await BackendApi.fetchJson(`/api/staff/${userId}`);
        return {
            id: user.id,
            avatar: user.avatar,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            phone: user.phone
        };
    }

    static async getUsers() {
        const users = await BackendApi.fetchJson('/api/staff');
        return users.map((u: ApiUserWithRoles) => ({
            id: u.user.id,
            firstname: u.user.firstname,
            lastname: u.user.lastname,
            email: u.user.email,
            phone: u.user.phone,
            roles: u.roles.map(r => r.code)
        }));
    }

    static async getRoles() {
        return await BackendApi.fetchJson('/api/staff/roles');
    }

    static async createUser(formdata: FormData) {
        return await BackendApi.fetchJson(`/api/staff`, {method: 'PUT', body: formdata});
    }

    static async deleteUser(userId: string|number) {
        return await BackendApi.fetchJson(`/api/staff/${userId}`, {method: 'DELETE'});
    }
    static async disableUser(userId: string|number) {
        return await BackendApi.fetchJson(`/api/staff/${userId}/disable`, {method: 'POST'});
    }

    static async setUserRoles(userId: string|number, roleCodes: string[], roles: ApiRole[]) {
        let role_ids = roleCodes.map(v => roles.find(r => r.code === v)!.id);
        return await BackendApi.fetchJson(`/api/staff/${userId}/roles`, {method: 'POST', body: JSON.stringify(role_ids)});
    }
}