import {decodeToken, isExpired} from "react-jwt";
import {StaffApi} from "./staffapi";

type AuthTokenType = {
    user_id: number,
    roles: string[]
};
type AuthUserType = {
    id: number,
    firstname: string,
    lastname: string,
    avatar: string
};

class Auth {
    public token: AuthTokenType | null;
    public user: AuthUserType | null;

    constructor() {
        this.token = null;
        this.user = null;

        const token = sessionStorage.getItem("jwt_token");
        if (token) {
            if (! isExpired(token)) {
                this.token = decodeToken(token) as AuthTokenType;
                this.user = JSON.parse(sessionStorage.getItem("pbm_user")!);
            }
        }
    }

    async setNewToken(token: string) {
        this.token = decodeToken(token) as AuthTokenType;

        const user = await StaffApi.getUser(this.user_id);
        this.user = user;

        sessionStorage.setItem("jwt_token", token);
        sessionStorage.setItem("pbm_user", JSON.stringify(user));
    }

    forgetToken() {
        sessionStorage.removeItem("jwt_token");
        sessionStorage.removeItem("pbm_user");
        this.token = null;
        this.user = null;
    }

    get isLoggedIn() {
        return this.token !== null;
    }

    get user_id() {
        return this.token!.user_id;
    }

    get roles() {
        if (this.token === null) {
            return [];
        }
        return this.token.roles;
    }
}

export default new Auth();