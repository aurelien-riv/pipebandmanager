import SnackbarHelper from "../common/SnackBarManager";

export class BackendApi {
    static async fetchAllowingErrors(input: RequestInfo, init?: RequestInit): Promise<Response> {
        try {
            return await fetch(input, init);
        } catch (e) {
            SnackbarHelper.error('Network error');
        }
        throw Error();
    }

    static async fetchJson(input: RequestInfo, init?: RequestInit): Promise<any> {
        try {
            const data = await fetch(input, init);
            if (data.ok) {
                return await data.json();
            } else {
                SnackbarHelper.error('Something went wrong');
            }
        } catch (e) {
            SnackbarHelper.error('Network error');
        }
        throw Error();
    }
}