import {DatePipe, registerLocaleData} from "@angular/common";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import localeFr from '@angular/common/locales/fr';
import {enableProdMode, importProvidersFrom, LOCALE_ID} from '@angular/core';
import {bootstrapApplication, BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule, NoopAnimationsModule} from "@angular/platform-browser/animations";
import {RouterModule, Routes} from "@angular/router";
import {HttpErrorObserver} from "./app/api/http-interceptors/http-error-observer";
import {AppComponent} from "./app/app.component";
import {NotificationService} from "./app/services/notification.service";
import { environment } from '@environment';
import {PageLoginComponent} from "./app/pages/auth/page-login.component";
import {PageUsersComponent} from "./app/pages/admin/users/page-users.component";
import {AppLayoutComponent} from "./app/pages/_layouts/app-layout.component";
import {AuthInterceptor} from "./app/auth/http-auth-observer";

registerLocaleData(localeFr);

const routes: Routes = [
  {
    path: 'auth',
    children: [{
      path: 'login', component: PageLoginComponent
    }]
  },
  {
    path: 'admin',
    component: AppLayoutComponent,
    children: [{
      path: 'users', component: PageUsersComponent // TODO authn + authz guard
    }]
  }
];

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(HttpClientModule),
    importProvidersFrom(BrowserModule),
    importProvidersFrom(BrowserAnimationsModule),
    // importProvidersFrom(NoopAnimationsModule),
    importProvidersFrom(RouterModule.forRoot(routes)),
    DatePipe,
    NotificationService,
    { provide: LOCALE_ID, useValue: "fr-FR" },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorObserver, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ]
}).catch(err => console.error(err));
