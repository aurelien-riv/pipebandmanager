import {HttpContextToken, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {inject, Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";

export const AUTHENTICATED = new HttpContextToken<boolean>(() => true);

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private readonly authService = inject(AuthService);

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (! req.context.get(AUTHENTICATED)) {
      return next.handle(req);
    }

    const modified = req.clone({setHeaders: { Authorization: this.authService.raw_token ?? ''}});
    return next.handle(modified);
  }
}
