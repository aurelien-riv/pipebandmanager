import {Injectable} from "@angular/core";

type JwtDataType = {
  iat: number;
  exp: number;
}
type AuthTokenType = {
  user_id: number,
  roles: string[]
};
type AuthUserType = {
  id: number,
  firstname: string,
  lastname: string,
  avatar: string
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public raw_token: string | null = null;
  public token: AuthTokenType | null = null;
  public user: AuthUserType | null = null;

  constructor() {
    const token = sessionStorage.getItem("jwt_token");
    if (token) {
      const tokenData = JSON.parse(atob(token.split('.')[1])) as JwtDataType & AuthTokenType;
      if ((tokenData.exp*1000) > new Date().getTime()) {
        this.raw_token = token;
        this.token = tokenData;
        // this.user = JSON.parse(sessionStorage.getItem("pbm_user")!);
      }
    }
  }

  async setNewToken(token: string) {
    this.token = JSON.parse(atob(token.split('.')[1])) as AuthTokenType;
    this.raw_token = token;
    console.log(this.token);
    // const user = await StaffApi.getUser(this.user_id);
    // this.user = user;

    sessionStorage.setItem("jwt_token", token);
    // sessionStorage.setItem("pbm_user", JSON.stringify(user));
  }

  forgetToken() {
    sessionStorage.removeItem("jwt_token");
    sessionStorage.removeItem("pbm_user");
    this.token = null;
    this.user = null;
  }

  get isLoggedIn() {
    return this.token !== null;
  }

  get user_id() {
    return this.token!.user_id;
  }

  get roles() {
    if (this.token === null) {
      return [];
    }
    return this.token.roles;
  }
}
