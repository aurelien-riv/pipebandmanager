export type ApiRole = {
  id: number,
  code: string,
  name: string
}
type ApiUser = {
  id: number,
  avatar: string,
  firstname: string,
  lastname: string,
  email: string,
  phone: string|null
};

export type ApiUserWithRoles = {
  user: ApiUser,
  roles: ApiRole[]
}
