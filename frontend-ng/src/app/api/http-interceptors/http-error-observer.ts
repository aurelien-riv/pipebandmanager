import {HttpContextToken, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpStatusCode} from "@angular/common/http";
import {inject, Injectable} from "@angular/core";
import {catchError, Observable, share} from "rxjs";
import {NotificationService} from "../../services/notification.service";

export const ALLOWED_STATUS_CODES = new HttpContextToken<Array<HttpStatusCode>>(() => []);

@Injectable()
export class HttpErrorObserver implements HttpInterceptor {
  private notificationService = inject(NotificationService);

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(req).pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (! req.context.get(ALLOWED_STATUS_CODES).includes(err.status)) {
            this.notificationService.notifyError('Une erreur est survenue');
            console.error(err);
          }
        } else {
          console.error("An unknown error occured", err);
        }
        throw err;
      }),
      share()
    );
  }
}
