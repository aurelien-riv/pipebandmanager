import {inject, Injectable} from "@angular/core";
import {HttpClient, HttpContext} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {AUTHENTICATED} from "../auth/http-auth-observer";

@Injectable({
  providedIn: 'root'
})
export class AuthApi {
  private readonly http = inject(HttpClient);

  public authenticate(email: string, password: string): Promise<string> {
    return firstValueFrom(this.http.post<string>(`/api/auth/login`, {
      email,
      password
    }, {
      context: new HttpContext().set(AUTHENTICATED, false)
    }))
  }
}
