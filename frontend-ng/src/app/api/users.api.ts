import {inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {firstValueFrom} from "rxjs";
import {ApiRole, ApiUserWithRoles} from "./type/user.type";

@Injectable({
  providedIn: 'root'
})
export class UsersApi {
  private readonly http = inject(HttpClient);

  public getUsers(): Promise<Array<ApiUserWithRoles>> {
    return firstValueFrom(this.http.get<Array<ApiUserWithRoles>>(`/api/staff`))
  }

  public getRoles(): Promise<Array<ApiRole>> {
    return firstValueFrom(this.http.get<Array<ApiRole>>(`/api/staff/roles`));
  }

  public setUserRoles(userId: number, roleIds: number[]): Promise<void> {
    return firstValueFrom(this.http.post<void>(`/api/staff/${userId}/roles`, roleIds));
  }

  public disable(userId: number): Promise<void> {
    return firstValueFrom(this.http.post<void>(`/api/staff/${userId}/disable`, null));
  }

  async delete(userId: number) {
    return firstValueFrom(this.http.delete<void>(`/api/staff/${userId}`));
  }
}
