import {inject, Injectable} from "@angular/core";

@Injectable()
export class NotificationService {

  public notifySuccess(summary: string, details?: string|undefined) {
  }

  public notifyWarning(summary: string, details?: string|undefined) {
  }

  public notifyError(summary: string, details?: string|undefined) {
  }
}
