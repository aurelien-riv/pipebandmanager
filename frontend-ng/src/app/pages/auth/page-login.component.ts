import {Component, inject, OnInit} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {AuthApi} from "../../api/auth.api";
import {AuthService} from "../../auth/auth.service";
import {Router} from "@angular/router";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";

@Component({
  selector: 'app-page-login',
  template: `
    <div class="login-area">
      <h2>Sign in</h2>
      <form (submit)="handleSubmit()">
        <mat-form-field>
          <mat-label>Email</mat-label>
            <input matInput
                   type="email"
                   name="email"
                   [(ngModel)]="email"
                   [ngModelOptions]="{standalone: true}"
            />
        </mat-form-field>

        <mat-form-field>
          <mat-label>Password</mat-label>
            <input matInput
                   type="password"
                   name="password"
                   [(ngModel)]="password"
                   [ngModelOptions]="{standalone: true}"
            />
        </mat-form-field>

        <button mat-raised-button color="primary" (click)="handleSubmit()">Submit</button>
      </form>
    </div>
  `,
  styles: [`
    :host {
      display: flex;
      justify-content: flex-end;
      background: url(https://theme.bordeaux-pipeband.fr/images/tartan.jpg);
    }

    .login-area {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      background: white;
      width: min(95%, 768px);
      height: 100vh;
    }

    form {
      display: flex;
      flex-direction: column;
      width: 80%;
    }
  `],
  imports: [
    FormsModule,
    MatInputModule,
    MatButtonModule
  ],
  standalone: true
})
export class PageLoginComponent implements OnInit {
  private readonly router = inject(Router);
  private readonly authService = inject(AuthService);
  private readonly authApi = inject(AuthApi);
  protected email = "";
  protected password = "";

  async ngOnInit() {
    if (this.authService.isLoggedIn) {
      await this.router.navigate(['']);
    }
  }

  async handleSubmit() {
    const token = await this.authApi.authenticate(this.email, this.password);
    await this.authService.setNewToken(token as string);
  }
}
