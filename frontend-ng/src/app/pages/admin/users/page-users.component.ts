import {AfterViewInit, Component, inject, ViewChild} from "@angular/core";
import {MatTableDataSource, MatTableModule} from "@angular/material/table";
import {UsersApi} from "../../../api/users.api";
import {ApiRole, ApiUserWithRoles} from "../../../api/type/user.type";
import {MatSort, MatSortModule} from "@angular/material/sort";
import {MatChipsModule} from "@angular/material/chips";
import {NgForOf} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";

@Component({
  selector: 'app-page-admin-users',
  template: `
    <table mat-table matSort [dataSource]="users">
      <ng-container matColumnDef="firstname">
        <th mat-header-cell *matHeaderCellDef  mat-sort-header>Firstname</th>
        <td mat-cell *matCellDef="let user"> {{user.user.firstname}} </td>
      </ng-container>
      <ng-container matColumnDef="lastname">
        <th mat-header-cell *matHeaderCellDef  mat-sort-header>Lastname</th>
        <td mat-cell *matCellDef="let user"> {{user.user.lastname}} </td>
      </ng-container>
      <ng-container matColumnDef="email">
        <th mat-header-cell *matHeaderCellDef  mat-sort-header>Adresse e-mail</th>
        <td mat-cell *matCellDef="let user"> {{user.user.email}} </td>
      </ng-container>
      <ng-container matColumnDef="phone">
        <th mat-header-cell *matHeaderCellDef  mat-sort-header>Telephone</th>
        <td mat-cell *matCellDef="let user"> {{user.user.phone}} </td>
      </ng-container>
      <ng-container matColumnDef="roles">
        <th mat-header-cell *matHeaderCellDef>Roles</th>
        <td mat-cell *matCellDef="let user">
          <mat-select multiple [ngModel]="user.roles" [compareWith]="compareRole" (ngModelChange)="updateRoles(user, $event)">
            <mat-option *ngFor="let role of roles" [value]="role.code">{{role.name}}</mat-option>
          </mat-select>
        </td>
      </ng-container>
      <ng-container matColumnDef="menu">
        <th mat-header-cell *matHeaderCellDef></th>
        <td mat-cell *matCellDef="let user">
          <button mat-icon-button [matMenuTriggerFor]="menu" aria-label="Example icon-button with a menu">
            <mat-icon>more_vert</mat-icon>
          </button>
          <mat-menu #menu="matMenu">
            <button mat-menu-item (click)="disableUser(user)">
              <mat-icon>pause_circle_outline</mat-icon>
              <span>Disable</span>
            </button>
            <button mat-menu-item (click)="deleteUser(user)">
              <mat-icon>delete_forever</mat-icon>
              <span>Delete</span>
            </button>
          </mat-menu>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="['firstname', 'lastname', 'email', 'phone', 'roles', 'menu']"></tr>
      <tr mat-row *matRowDef="let myRowData; columns: ['firstname', 'lastname', 'email', 'phone', 'roles', 'menu']"></tr>
    </table>
  `,
  imports: [
    MatTableModule,
    MatSortModule,
    NgForOf,
    MatSelectModule,
    FormsModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
  ],
  standalone: true
})
export class PageUsersComponent implements AfterViewInit {
  private readonly usersApi = inject(UsersApi);

  @ViewChild(MatSort) sort!: MatSort;

  protected users!: MatTableDataSource<ApiUserWithRoles>;
  protected roles!: Array<ApiRole>;

  public async ngAfterViewInit() {
    this.roles = await this.usersApi.getRoles();

    this.users = new MatTableDataSource(await this.usersApi.getUsers());
    this.users.sortingDataAccessor = (item, property) => {
      return (item.user as Record<string, string|number>)[property];
    }
    this.users.sort = this.sort;
  }

  protected async updateRoles(user: ApiUserWithRoles, roles: Array<string>) {
    user.roles = this.roles.filter(r => roles.includes(r.code));
    await this.usersApi.setUserRoles(user.user.id, user.roles.map(r => r.id));
  }

  protected async disableUser(user: ApiUserWithRoles) {
    await this.usersApi.disable(user.user.id); // todo identifier si disabled pourrait être pratique ^^
  }

  protected async deleteUser(user: ApiUserWithRoles) {
    await this.usersApi.delete(user.user.id);
    this.users.data.splice(this.users.data.indexOf(user), 1);
    this.users._updateChangeSubscription();
  }

  protected compareRole(r1: string, r2: ApiRole): boolean {
    return r1 === r2.code;
  }
}
