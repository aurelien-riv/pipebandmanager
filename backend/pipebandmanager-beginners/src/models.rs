use diesel::Queryable;
use rocket::serde::{Serialize, Deserialize};
use pipebandmanager_core::schema::*;

pub mod global_skill_progression;
pub mod beginner_note;

#[derive(Clone, Serialize, Deserialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct SkillGroup {
    pub id: i32,
    pub name: String
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct Skill {
    pub id: i32,
    pub group_id: i32,
    pub name: String
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct UserSkill {
    pub user_id: i32,
    pub skill_id: i32,
    pub level: i32
}
