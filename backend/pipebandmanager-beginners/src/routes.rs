mod skill;
mod beginner_notes;
mod beginners;

pub fn get_api_routes() -> Vec<rocket::Route> {
    routes![
        skill::get_skills,
        skill::level::set_skill_level,
        skill::global_progression::global_skill_progression,
        beginner_notes::get_notes,
        beginner_notes::add_note,
        beginner_notes::edit_note,
        beginners::beginners_list
    ]
}