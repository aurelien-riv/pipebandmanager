use diesel::{Queryable, RunQueryDsl, sql_query};
use chrono::NaiveDate;
use diesel::sql_types::Integer;
use rocket::serde::{Serialize, Deserialize};
use pipebandmanager_core::schema::*;

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct GlobalSkillProgression {
    pub user_id: i32,
    pub date: NaiveDate,
    pub score: i32,
    pub max_score: i32
}
impl GlobalSkillProgression {
    pub fn refresh(user_id: i32, cnx: &mut diesel::SqliteConnection) {
        sql_query("
            REPLACE INTO global_skill_progressions (user_id, date, score, max_score)
            VALUES (
                :user_id,
                DATE(),
                (SELECT SUM(level) FROM user_skills WHERE user_id = :user_id),
                (SELECT COUNT(*)*4 FROM skills)
            )")
            .bind::<Integer, _>(user_id)
            .execute(cnx).unwrap();
    }
}
