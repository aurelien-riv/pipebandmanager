use diesel::Queryable;
use chrono::{NaiveDate, NaiveDateTime, Utc};
use rocket::serde::{Serialize, Deserialize};
use pipebandmanager_core::schema::*;

#[derive(Serialize, Deserialize, Queryable)]
#[serde(crate = "rocket::serde")]
pub struct BeginnerNote {
    pub id: i32,
    pub user_id: i32,
    pub teacher_id: i32,
    pub date: NaiveDate,
    pub message: String,

    pub created_at: NaiveDateTime,
    pub updated_at: Option<NaiveDateTime>,
    pub deleted_at: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Insertable)]
#[diesel(table_name = beginner_notes)]
#[serde(crate = "rocket::serde")]
pub struct NewBeginnerNote {
    pub user_id: i32,
    pub teacher_id: i32,
    pub date: NaiveDate,
    pub message: String,
    pub created_at: NaiveDateTime
}
impl NewBeginnerNote {
    pub fn new(user_id: i32, teacher_id: i32, message: String) -> NewBeginnerNote {
        NewBeginnerNote {
            user_id,
            teacher_id,
            message,
            date: Utc::now().naive_utc().date(),
            created_at: Utc::now().naive_utc()
        }
    }
}