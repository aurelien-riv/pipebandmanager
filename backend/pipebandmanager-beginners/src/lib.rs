#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;

pub mod models;
pub mod routes;
pub mod helpers;

