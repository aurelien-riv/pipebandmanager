use diesel::prelude::*;
use rocket::serde::{Serialize, Deserialize};
use crate::models::{Skill, SkillGroup, UserSkill};
use pipebandmanager_core::schema::{skill_groups, skills, user_skills};

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct SkillWithLevel {
    pub skill_id: i32,
    pub skill_name: String,
    pub level: i32
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct MusicianSkillgroup {
    pub group_id: i32,
    pub group_name: String,
    pub skills: Vec<SkillWithLevel>
}
impl MusicianSkillgroup {
    pub fn new(group: SkillGroup, user_id: i32, cnx: &mut diesel::SqliteConnection) -> MusicianSkillgroup {
        let mut skillgrp = MusicianSkillgroup {
            group_id: group.id,
            group_name: group.name,
            skills: Vec::new()
        };

        for skill in skills::table.filter(skills::group_id.eq(skillgrp.group_id)).load::<Skill>(cnx).unwrap() {
            // FIXME suboptimal
            let level = user_skills::table
                .filter(user_skills::user_id.eq(user_id))
                .filter(user_skills::skill_id.eq(skill.id))
                .first::<UserSkill>(cnx)
                .map_or(0, |us| us.level);

            skillgrp.skills.push(SkillWithLevel {
                skill_id: skill.id,
                skill_name: skill.name,
                level
            });
        }

        skillgrp
    }
}

pub fn get_all_skill_groups(user_id: i32, cnx: &mut diesel::SqliteConnection) -> Vec<MusicianSkillgroup> {
    skill_groups::table.load::<SkillGroup>(cnx).unwrap()
        .iter().cloned()
        .map(|sg: SkillGroup| MusicianSkillgroup::new(sg, user_id, cnx))
        .collect::<Vec<_>>()
}