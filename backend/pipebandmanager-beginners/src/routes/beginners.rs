use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
use rocket::response::status;
use rocket::serde::json::Json;
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::models::user::User;
use pipebandmanager_core::schema::roles;
use pipebandmanager_core::schema::user_roles;
use pipebandmanager_core::schema::users;

#[get("/students")]
pub async fn beginners_list(auth: UserToken) -> Result<Json<Vec<User>>, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }

    let students = users::table
        .select(users::all_columns)
        .inner_join(user_roles::table.inner_join(roles::table))
        .filter(roles::code.eq(&"beginner"))
        .load::<User>(&mut cnx)
        .unwrap();

    Ok(Json(students))
}