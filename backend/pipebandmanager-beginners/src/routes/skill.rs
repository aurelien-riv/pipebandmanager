use rocket::response::status;
use rocket::serde::json::Json;
use pipebandmanager_core::database::get_db;

use pipebandmanager_core::models::auth::UserToken;

use crate::helpers::musician_skill_group::{get_all_skill_groups, MusicianSkillgroup};

pub mod level;
pub mod global_progression;

#[get("/<user_id>/skills")]
pub async fn get_skills(user_id: i32, auth: UserToken) -> Result<Json<Vec<MusicianSkillgroup>>, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if auth.user_id != user_id && ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }

    Ok(Json(get_all_skill_groups(user_id, &mut cnx)))
}
