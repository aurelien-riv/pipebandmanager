use chrono::{NaiveDate, Utc};
use diesel::insert_into;
use rocket::response::status;
use diesel::update;
use diesel::prelude::*;
use rocket::serde::json::Json;
use rocket::serde::Serialize;
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::models::user::User;
use pipebandmanager_core::schema::{beginner_notes, users};
use crate::models::beginner_note::{BeginnerNote, NewBeginnerNote};

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct BeginnerNoteWithTeacher {
    pub id: i32,
    pub user_id: i32,
    pub date: NaiveDate,
    pub message: String,
    pub teacher: User
}
impl BeginnerNoteWithTeacher {
    fn from_query_result(n: &BeginnerNote, t: &User) -> BeginnerNoteWithTeacher {
        BeginnerNoteWithTeacher {
            id: n.id,
            user_id: n.user_id,
            date: n.date,
            message: n.message.clone(),
            teacher: t.clone()
        }
    }
}

#[get("/<user_id>/notes")]
pub async fn get_notes(user_id: i32, auth: UserToken) -> Result<Json<Vec<BeginnerNoteWithTeacher>>, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if user_id != auth.user_id && ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }

    let notes = beginner_notes::table
        .inner_join(users::table::on(users::table, beginner_notes::teacher_id.eq(users::id)))
        .filter(beginner_notes::user_id.eq(&user_id))
        .filter(beginner_notes::deleted_at.is_null())
        .order(beginner_notes::date.desc())
        .order(beginner_notes::created_at.desc())
        .load::<(BeginnerNote, User)>(&mut cnx).unwrap()
        .iter().map(|(n,u)| BeginnerNoteWithTeacher::from_query_result(n, u))
        .collect();

    Ok(Json(notes))
}

#[put("/<user_id>/notes", data = "<message>")]
pub async fn add_note(user_id: i32, auth: UserToken, message: String) -> Result<Json<BeginnerNoteWithTeacher>, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if ! auth.has_role("teacher") {
            return Err(status::Unauthorized::<()>(None));
        }

        insert_into(beginner_notes::table)
            .values(&NewBeginnerNote::new(user_id, auth.user_id, message))
            .execute(&mut cnx)
            .unwrap();

        let (n, u) = beginner_notes::table
            .inner_join(users::table::on(users::table, beginner_notes::teacher_id.eq(users::id)))
            .order(beginner_notes::id.desc())
            .first::<(BeginnerNote, User)>(&mut cnx)
            .unwrap();
        Ok(Json(BeginnerNoteWithTeacher::from_query_result(&n, &u)))
}
#[patch("/<_user_id>/note/<note_id>", data = "<message>")]
pub async fn edit_note(_user_id: i32, note_id: i32, auth: UserToken, message: String) -> Result<status::NoContent, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }
    let exists = beginner_notes::table
        .filter(beginner_notes::id.eq(&note_id))
        .filter(beginner_notes::teacher_id.eq(&auth.user_id))
        .first::<BeginnerNote>(&mut cnx)
        .is_ok();

    if ! exists {
        return Err(status::Unauthorized::<()>(None));
    }

    update(beginner_notes::table.filter(beginner_notes::id.eq(&note_id)))
        .set((
                 beginner_notes::message.eq(&message),
                 beginner_notes::updated_at.eq(&Utc::now().naive_utc())
        ))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}