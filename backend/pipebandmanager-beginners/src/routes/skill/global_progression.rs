use rocket::response::status;
use diesel::prelude::*;
use rocket::serde::json::Json;
use pipebandmanager_core::database::get_db;
use crate::models::global_skill_progression::GlobalSkillProgression;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::global_skill_progressions;

#[get("/<user_id>/global_skill_progression")]
pub async fn global_skill_progression(user_id: i32, auth: UserToken) -> Result<Json<Vec<GlobalSkillProgression>>, status::Unauthorized<()>>
{
    let mut cnx = get_db();
    if user_id != auth.user_id && ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }

    let progression = global_skill_progressions::table
        .filter(global_skill_progressions::user_id.eq(&user_id))
        .load::<GlobalSkillProgression>(&mut cnx).unwrap();

    Ok(Json(progression))
}