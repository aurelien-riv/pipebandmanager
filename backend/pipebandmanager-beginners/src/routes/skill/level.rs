use rocket::response::status;
use diesel::prelude::*;
use pipebandmanager_core::database::get_db;
use crate::models::UserSkill;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::user_skills;
use crate::models::global_skill_progression::GlobalSkillProgression;

#[post("/<user_id>/skill/<skill_id>/level/<level>")]
pub async fn set_skill_level(user_id: i32, skill_id: i32, level: i32, auth: UserToken) -> Result<status::Accepted<()>, status::Unauthorized<()>> {
    let mut cnx = get_db();

    if ! auth.has_role("teacher") {
        return Err(status::Unauthorized::<()>(None));
    }

    diesel::replace_into(user_skills::table)
        .values(&UserSkill {user_id, skill_id, level})
        .execute(&mut cnx)
        .unwrap();

    GlobalSkillProgression::refresh(user_id, &mut cnx);

    Ok(status::Accepted::<()>(None))
}