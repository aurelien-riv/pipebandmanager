#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel_migrations;

use rocket::{Build, Rocket};
use rocket::fairing::AdHoc;

mod react;
mod migrations;

#[launch]
fn rocket() -> Rocket<Build> {
    rocket::build()
        .attach(AdHoc::on_ignite("Diesel Migrations", migrations::run_migrations))
        .mount("/", routes![react::index, react::files, react::fallback_url])
        .mount("/api/staff", pipebandmanager_staff::routes::get_api_routes())
        .mount("/api/auth", pipebandmanager_auth::routes::get_api_routes())
        .mount("/api/beginners", pipebandmanager_beginners::routes::get_api_routes())
}