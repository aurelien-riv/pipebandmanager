use std::borrow::Cow;
use std::path::PathBuf;
use rust_embed::{EmbeddedFile, RustEmbed};
use rocket::http::{ContentType, Status};

#[derive(RustEmbed)]
#[folder = "$CARGO_MANIFEST_DIR/../frontend/dist"]
#[exclude = "*.txt"]
struct Asset;

#[get("/")]
pub async fn index<'r>() -> Result<(ContentType, Cow<'static, [u8]>), Status> {
    if let Some(file) = Asset::get("index.html") as Option<EmbeddedFile> {
        Ok((ContentType::HTML, file.data))
    } else {
        Err(Status::NotFound)
    }
}

#[get("/<path..>", rank=1)]
pub async fn files<'r>(path: PathBuf) -> Result<(ContentType, Cow<'static, [u8]>), Status> {
    if let Some(file) = Asset::get(path.to_str().unwrap()) {
        let ext = path.extension().unwrap().to_str().unwrap();
        let content_type = ContentType::from_extension(ext).unwrap();
        Ok((content_type, file.data))
    } else {
        Err(Status::NotFound)
    }
}

#[get("/<_..>", rank=2)]
pub async fn fallback_url<'r>() -> Result<(ContentType, Cow<'static, [u8]>), Status> {
    index().await
}
