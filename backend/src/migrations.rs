use std::fs;
use std::os::unix::fs::PermissionsExt;
use diesel_migrations::{MigrationHarness, EmbeddedMigrations};
use rocket::{Rocket, Build};
use pipebandmanager_core::database::{get_db, get_db_path};

pub async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");
    get_db().run_pending_migrations(MIGRATIONS).unwrap();
    secure_db_files();
    rocket
}

fn secure_db_files() {
    let db_filename = get_db_path();
    fs::set_permissions(&db_filename, fs::Permissions::from_mode(0o600))
        .unwrap_or_else(|_| panic!("Failed to set permissions 0600 on file {}", &db_filename));
}