use diesel::prelude::*;
use rocket::response::status;
use rocket::serde::{json::Json};
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::models::role::Role;
use pipebandmanager_core::schema::roles;

#[rocket::get("/roles")]
pub async fn get_roles(auth: UserToken) -> Result<Json<Vec<Role>>, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }

    let mut cnx = get_db();
    Ok(Json(roles::table.load(&mut cnx).unwrap()))
}