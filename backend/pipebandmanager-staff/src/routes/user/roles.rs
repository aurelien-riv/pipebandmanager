use diesel::{delete, insert_into};
use diesel::prelude::*;
use rocket::response::status;
use rocket::serde::{json::Json};
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::user_roles;

#[rocket::post("/<user_id>/roles", data="<role_ids>")]
pub async fn set_user_roles(user_id: i32, auth: UserToken, role_ids: Json<Vec<i32>>) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }

    let data = role_ids.iter().map(|&role_id| (
        user_roles::user_id.eq(user_id),
        user_roles::role_id.eq(role_id))
    ).collect::<Vec<_>>();

    let mut cnx = get_db();

    cnx.transaction::<_, diesel::result::Error, _>(|conn| {
        delete(user_roles::table.filter(user_roles::user_id.eq(user_id))).execute(conn)?;
        insert_into(user_roles::table).values(&data).execute(conn)?;
        Ok(())
    }).unwrap();

    Ok(status::NoContent)
}


