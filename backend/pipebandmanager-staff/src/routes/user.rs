use diesel::{insert_into, update};
use diesel::prelude::*;
use rocket::form::Form;
use rocket::response::status;
use rocket::serde::{json::Json, Serialize};
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::models::role::Role;
use pipebandmanager_core::models::user::User;
use pipebandmanager_core::schema::users;

pub mod roles;

#[derive(Insertable, FromForm)]
#[diesel(table_name = users)]
pub struct NewUser {
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub phone: Option<String>
}
impl NewUser {
    fn cleanup(&mut self) {
        if self.phone.is_some() && self.phone.as_ref().unwrap().is_empty() {
            self.phone = None;
        }
    }
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct UserWithRoles {
    user: User,
    roles: Vec<Role>
}

#[rocket::get("/<user_id>")]
pub async fn get_user(user_id: i32, _auth: UserToken) -> Json<User> {
    let mut cnx = get_db();
    Json(User::find_by_id(user_id, &mut cnx).unwrap())
}

#[rocket::get("/")]
pub async fn get_users(auth: UserToken) -> Result<Json<Vec<UserWithRoles>>, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }

    let mut cnx = get_db();
    Ok(Json(
        users::table
            .filter(users::deleted_at.is_null())
            .load(&mut cnx).unwrap()
            .iter()
            .map(|user: &User| UserWithRoles { user: user.clone(), roles: user.get_roles(&mut cnx)})
            .collect()
    ))
}

#[rocket::put("/", data="<user>")]
pub async fn create_user(user: Form<NewUser>, auth: UserToken) -> Result<Json<User>, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }

    let mut user = user.into_inner();
    user.cleanup();

    let mut cnx = get_db();

    insert_into(users::table)
        .values(&user)
        .execute(&mut cnx)
        .unwrap();

    let u = users::table
        .order(users::id.desc())
        .first::<User>(&mut cnx)
        .unwrap();
    Ok(Json(u))
}

#[rocket::delete("/<user_id>")]
pub async fn del_user(user_id: i32, auth: UserToken) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }
    let mut cnx = get_db();

    update(users::table.filter(users::id.eq(&user_id)))
        .set(users::deleted_at.eq(chrono::Utc::now().naive_utc()))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}

#[rocket::post("/<user_id>/disable")]
pub async fn disable_user(user_id: i32, auth: UserToken) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") {
        return Err(status::Unauthorized::<()>(None));
    }
    let mut cnx = get_db();

    update(users::table.filter(users::id.eq(&user_id)))
        .set(users::active.eq(0))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}


