extern crate base64;
use diesel::{update};
use diesel::prelude::*;
use rocket::data::{Data, ToByteUnit};
use rocket::response::status;
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::{users};
use base64::{Engine as _, engine::general_purpose};

#[rocket::post("/<user_id>/profile/avatar", data="<image>")]
pub async fn set_avatar(user_id: i32, auth: UserToken, image: Data<'_>) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") && auth.user_id != user_id {
        return Err(status::Unauthorized::<()>(None));
    }

    let image_data = image.open(128.kibibytes()).into_bytes().await;
    let image_b64 = general_purpose::STANDARD.encode(image_data.unwrap().value);

    let mut cnx = get_db();
    update(users::table.filter(users::id.eq(&user_id)))
        .set(users::avatar.eq(&image_b64))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}


