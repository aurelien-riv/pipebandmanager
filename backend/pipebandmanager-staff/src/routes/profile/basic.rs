use diesel::{update};
use diesel::prelude::*;
use rocket::response::status;
use rocket::serde::{json::Json, Deserialize};
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::{users};

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct BasicProfileData {
    pub firstname: String,
    pub lastname: String,
    pub email: String
}

#[rocket::post("/<user_id>/profile/basic", data="<data>")]
pub async fn set_basic(user_id: i32, auth: UserToken, data: Json<BasicProfileData>) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") && auth.user_id != user_id {
        return Err(status::Unauthorized::<()>(None));
    }

    let mut cnx = get_db();
    update(users::table.filter(users::id.eq(&user_id)))
        .set((
             users::firstname.eq(&data.firstname),
             users::lastname.eq(&data.lastname),
             users::email.eq(&data.email)
         ))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}


