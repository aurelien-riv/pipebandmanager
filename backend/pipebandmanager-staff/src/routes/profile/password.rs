extern crate base64;
use diesel::update;
use diesel::prelude::*;
use rocket::response::status;
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::schema::users;

#[rocket::post("/<user_id>/profile/password", data="<password>")]
pub async fn set_password(user_id: i32, auth: UserToken, password: String) -> Result<status::NoContent, status::Unauthorized<()>> {
    if ! auth.has_role("admin") && auth.user_id != user_id {
        return Err(status::Unauthorized::<()>(None));
    }

    // fixme hashing operation should be moved to the auth workspace
    let password = bcrypt::hash(password, 10).unwrap();

    let mut cnx = get_db();

    update(users::table.filter(users::id.eq(&user_id)))
        .set(users::password.eq(&password))
        .execute(&mut cnx).unwrap();

    Ok(status::NoContent)
}


