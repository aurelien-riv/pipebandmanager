mod user;
mod roles;
mod profile;

pub fn get_api_routes() -> Vec<rocket::Route> {
    routes![
        user::get_user,
        user::get_users,
        user::del_user,
        user::create_user,
        user::disable_user,
        user::roles::set_user_roles,
        roles::get_roles,
        profile::basic::set_basic,
        profile::avatar::set_avatar,
        profile::password::set_password
    ]
}