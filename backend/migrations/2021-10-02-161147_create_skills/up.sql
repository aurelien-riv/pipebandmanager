CREATE TABLE skill_groups (
   id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
   name TEXT NOT NULL
);

CREATE TABLE skills (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    group_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    FOREIGN KEY(group_id) REFERENCES skill_groups(id)
);

CREATE TABLE user_skills (
    user_id INTEGER NOT NULL,
    skill_id INTEGER NOT NULL,
    level UNSIGNED INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY (user_id, skill_id),
    FOREIGN KEY(user_id) REFERENCES users(id),
    FOREIGN KEY(skill_id) REFERENCES skills(id)
);

INSERT INTO skill_groups VALUES
    (1, "Solfège"),
    (2, "Practice"),
    (3, "Bagpipes"),
    (4, "Formation");

INSERT INTO skills (group_id, name) VALUES
    (1, "Lire les notes"),
    (1, "Lire le rythme"),
    (2, "Gamme"),
    (2, "Détachés"),
    (2, "Battements"),
    (2, "GDE"),
    (2, "Tachum"),
    (2, "Throw on D"),
    (2, "Lemluath"),
    (2, "Taorluath"),
    (2, "Birl"),
    (2, "Rodin"),
    (2, "Darodo"),
    (3, "Assemblage"),
    (3, "Entretien"),
    (3, "Accord des bourdons"),
    (3, "Accord du chanter"),
    (3, "Réglage des bourdons"),
    (3, "Départs des arrêts"),
    (3, "Pression d'air"),
    (4, "S'habiller"),
    (4, "Les ordres"),
    (4, "Posture statique"),
    (4, "Marche"),
    (4, "Tiroirs");
