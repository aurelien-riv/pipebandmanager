CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    active INTEGER(1) NOT NULL DEFAULT 1,
    password CHAR(76),
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    phone TEXT NULL UNIQUE,
    avatar TEXT NULL,
    deleted_at DATETIME NULL
);

CREATE TABLE roles (
    id INTEGER PRIMARY KEY NOT NULL,
    code TEXT NOT NULL,
    name TEXT NOT NULL
);

CREATE TABLE user_roles (
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY(user_id) REFERENCES users(id)
    FOREIGN KEY(role_id) REFERENCES roles(id)
);

INSERT INTO roles VALUES
     (1, "admin",    "Administrateur"),
     (2, "manager",  "Gestionaire"),
     (3, "musician", "Musicien"),
     (4, "beginner", "Débutant"),
     (5, "teacher",  "Formateur");