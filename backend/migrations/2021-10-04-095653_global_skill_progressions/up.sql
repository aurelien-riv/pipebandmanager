CREATE TABLE global_skill_progressions (
    user_id INTEGER NOT NULL,
    date DATE NOT NULL,
    score INTEGER NOT NULL,
    max_score INTEGER NOT NULL,
    PRIMARY KEY (user_id, date)
);