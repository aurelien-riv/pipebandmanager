#[macro_use] extern crate rocket;

use rocket::serde::Deserialize;
pub mod routes;

#[derive(FromForm, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct LoginForm {
    pub email: String,
    pub password: String,
}

// todo expose a password encoder so that all password hashing (bcrypt) operations remains in the auth module