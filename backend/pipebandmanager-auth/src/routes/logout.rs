use rocket::response::status;
use rocket::http::CookieJar;
use pipebandmanager_core::models::auth::UserToken;

#[get("/logout")]
pub async fn logout(jar: &CookieJar<'_>) -> status::Accepted<()> {
    UserToken::logout(jar);
    status::Accepted(Some(()))
}