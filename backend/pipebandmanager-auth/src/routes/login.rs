use rocket::response::status;
use rocket::http::{CookieJar};
use rocket::serde::json::Json;
use pipebandmanager_core::database::get_db;
use pipebandmanager_core::models::auth::UserToken;
use pipebandmanager_core::models::user::User;
use crate::LoginForm;

fn validate_password(password: &String, form_password: &str) -> Result<(), &'static str> {
    match bcrypt::verify(&form_password, password) {
        Ok(valid) if valid => Ok(()),
        Ok(_) => Err("Invalid email or password"),
        Err(_) => Err("Password verification failed")
    }
}

#[post("/login", data = "<form>")]
pub async fn login(form: Json<LoginForm>, jar: &CookieJar<'_>) -> Result<status::Accepted<Json<String>>, status::Unauthorized<Json<String>>> {
    let mut cnx = get_db();
    if let Ok(user) = User::find_active_user_by_email(form.email.clone(), &mut cnx) {
        match validate_password(&user.password.clone().unwrap(), &form.password) {
            Ok(_) => {
                let roles = user.get_roles(&mut cnx).iter().map(|r| r.code.clone()).collect();
                let token = UserToken::new(user, roles).authenticate(jar);
                Ok(status::Accepted(Some(Json(token))))
            },
            Err(msg) => Err(status::Unauthorized(Some(Json(msg.to_string()))))
        }
    } else {
        Err(status::Unauthorized(Some(Json("Invalid email or password".to_string()))))
    }
}