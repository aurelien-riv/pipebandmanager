mod login;
mod logout;

pub fn get_api_routes() -> Vec<rocket::Route> {
    routes![login::login, logout::logout]
}