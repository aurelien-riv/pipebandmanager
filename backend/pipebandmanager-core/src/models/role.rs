use diesel::Queryable;
use rocket::serde::{Serialize, Deserialize};

use crate::schema::*;

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct Role {
    pub id: i32,
    pub code: String,
    pub name: String
}