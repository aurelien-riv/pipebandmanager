use chrono::Utc;
use jsonwebtoken::errors::Result;
use jsonwebtoken::TokenData;
use jsonwebtoken::{Header, Validation};
use jsonwebtoken::{EncodingKey, DecodingKey};
use rocket::http::{Cookie, CookieJar, Status};
use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::response::status;
use rocket::serde::{Serialize, Deserialize, json::{Json}};
use crate::models::user::User;

static ONE_WEEK: i64 = 60 * 60 * 24 * 7; // in seconds

static AUTH_JWT_COOKIE_NAME: &str = "auth_jwt";

#[derive(Debug, Serialize, Deserialize)]
pub struct Response {
    pub message: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserToken {
    // issued at
    pub iat: i64,
    // expiration
    pub exp: i64,
    // data
    pub user_id: i32,
    pub user_email: String,
    pub roles: Vec<String>
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserToken {
    type Error = status::Custom<Json<Response>>;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let token = request.cookies()
            .get_private(AUTH_JWT_COOKIE_NAME)
            .and_then(|cookie| UserToken::decode_token(cookie.value().to_owned()).ok());

        if let Some(token_data) = token {
            return Outcome::Success(token_data.claims);
        }

        return Outcome::Failure((
            Status::BadRequest,
            status::Custom(
                Status::Unauthorized,
                Json(Response { message: String::from("No token provided or invalid token") }),
            ),
        ));
    }
}

impl UserToken {
    pub fn new(user: User, roles: Vec<String>) -> UserToken {
        let now = Utc::now().timestamp_nanos() / 1_000_000_000; // nanosecond -> second
        UserToken {
            iat: now,
            exp: now + ONE_WEEK,
            user_id: user.id,
            user_email: user.email,
            roles
        }
    }

    pub fn authenticate(&self, jar: &CookieJar<'_>) -> String {
        let token = self.generate_token();
        jar.add_private(Cookie::new(AUTH_JWT_COOKIE_NAME, token.clone()));
        token
    }

    pub fn logout(jar: &CookieJar<'_>) {
        if let Some(c) = jar.get_private(AUTH_JWT_COOKIE_NAME) {
            jar.remove_private(c);
        }
    }

    pub fn generate_token(&self) -> String {
        // FIXME danger, the key should be defined the a config file so that it is not common to all deployments/instances
        jsonwebtoken::encode(&Header::default(), self, &EncodingKey::from_secret(include_bytes!("auth.key"))).unwrap()
    }

    pub fn has_role(&self, role: &str) -> bool {
        self.roles.contains(&role.to_owned())
    }

    pub fn decode_token(token: String) -> Result<TokenData<UserToken>> {
        // FIXME danger, the key should be defined the a config file so that it is not common to all deployments/instances
        jsonwebtoken::decode::<UserToken>(&token, &DecodingKey::from_secret(include_bytes!("auth.key")), &Validation::default())
    }
}