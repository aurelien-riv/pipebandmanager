use chrono::NaiveDateTime;
use diesel::prelude::*;
use rocket::serde::{Serialize, Deserialize};
use crate::schema::*;
use crate::models::role::Role;

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
pub struct User {
    pub id: i32,
    pub active: i32,
    #[serde(skip_serializing)]
    pub password: Option<String>,
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub phone: Option<String>,
    pub avatar: Option<String>,
    pub deleted_at: Option<NaiveDateTime>
}
impl User {
    pub fn find_by_id(id: i32, cnx: &mut diesel::SqliteConnection) -> Result<User, diesel::result::Error> {
        users::table.filter(users::id.eq(&id).and(users::deleted_at.is_null())).first::<User>(cnx)
    }

    pub fn find_active_user_by_email(email: String, cnx: &mut diesel::SqliteConnection) -> Result<User, diesel::result::Error> {
        users::table
            .filter(users::email.eq(&email))
            .filter(users::password.is_not_null())
            .filter(users::active.eq(&1))
            .first::<User>(cnx)
    }

    pub fn get_roles(&self, cnx: &mut diesel::SqliteConnection) -> Vec<Role> {
        user_roles::table.filter(user_roles::user_id.eq(self.id))
            .inner_join(roles::table)
            .select(roles::all_columns)
            .load::<Role>(cnx)
            .unwrap()
    }
}