table! {
    beginner_notes (id) {
        id -> Integer,
        user_id -> Integer,
        teacher_id -> Integer,
        date -> Date,
        message -> Text,
        created_at -> Timestamp,
        updated_at -> Nullable<Timestamp>,
        deleted_at -> Nullable<Timestamp>,
    }
}

table! {
    global_skill_progressions (user_id, date) {
        user_id -> Integer,
        date -> Date,
        score -> Integer,
        max_score -> Integer,
    }
}

table! {
    roles (id) {
        id -> Integer,
        code -> Text,
        name -> Text,
    }
}

table! {
    skill_groups (id) {
        id -> Integer,
        name -> Text,
    }
}

table! {
    skills (id) {
        id -> Integer,
        group_id -> Integer,
        name -> Text,
    }
}

table! {
    user_roles (user_id, role_id) {
        user_id -> Integer,
        role_id -> Integer,
    }
}

table! {
    user_skills (user_id, skill_id) {
        user_id -> Integer,
        skill_id -> Integer,
        level -> Integer,
    }
}

table! {
    users (id) {
        id -> Integer,
        active -> Integer,
        password -> Nullable<Text>,
        firstname -> Text,
        lastname -> Text,
        email -> Text,
        phone -> Nullable<Text>,
        avatar -> Nullable<Text>,
        deleted_at -> Nullable<Timestamp>,
    }
}

joinable!(beginner_notes -> users (teacher_id));
joinable!(skills -> skill_groups (group_id));
joinable!(user_roles -> roles (role_id));
joinable!(user_roles -> users (user_id));
joinable!(user_skills -> skills (skill_id));
joinable!(user_skills -> users (user_id));

allow_tables_to_appear_in_same_query!(
    beginner_notes,
    global_skill_progressions,
    roles,
    skill_groups,
    skills,
    user_roles,
    user_skills,
    users,
);
