use rocket::config::Config;
use serde::Deserialize;
use diesel::{Connection, SqliteConnection};

pub type DbCnx = SqliteConnection;

#[derive(Deserialize)]
struct CnxDbSettings {
    url: String
}

pub fn get_db_path() -> String {
    if let Ok(config) = Config::figment().focus("databases.db").extract::<CnxDbSettings>() {
        config.url
    } else {
        panic!("No database url given, or Rocket.toml file not found");
    }
}

pub fn get_db() -> DbCnx {
    SqliteConnection::establish(&get_db_path()).unwrap()
}
